const express = require("express");
const app = express();
const axios = require("axios");
const moment = require("moment");

const _host = "e.khanbank.com";
var _account = "5675458490";
var isRefreshing = false;

const getNow = () => {
  return Date.now();
};

var _object = {
  access_token: "",
  access_token_expires_in: getNow(),
};

const _headers = {
  Accept: "application/json, text/plain, */*",
  "Accept-Encoding": "gzip, deflate, br, zstd",
  "Accept-Language": "mn-MN",
  Connection: "keep-alive",
  "Content-Length": "112",
  "Content-Type": "application/json",
  Cookie:
    "__ssds=2; __ssuzjsr2=a9be0cd8e; __uzmaj2=15e3e965-de2b-464d-bd25-ac1c56858e5f; __uzmbj2=1712106543; _ga=GA1.1.1821477585.1712106538; __uzma=5eb845c0-8464-0ac2-c6d8-bd1bdaa6cc4e; __uzmb=1713494395; __uzme=8088; SL_ClassKey=0.1.1; _ga_XY11GTD04T=GS1.1.1716343619.3.0.1716343623.56.0.0; __uzmcj2=143393448285; __uzmdj2=1716349387; __uzmc=3753521145555; __uzmd=1716350094",
  Host: "e.khanbank.com",
  Origin: "https://e.khanbank.com",
  Referer: "https://e.khanbank.com/auth/login",
  "Sec-Fetch-Dest": "empty",
  "Sec-Fetch-Mode": "cors",
  "Sec-Fetch-Site": "same-origin",
  "User-Agent":
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36",
  "app-version": "1.3.48-rc.718",
  channelcontext: "",
  "device-id": "04CF8760-38BF-42DF-A98E-3EA7A4CB756E",
  "sec-ch-ua":
    '"Chromium";v="124", "Google Chrome";v="124", "Not-A.Brand";v="99"',
  "sec-ch-ua-mobile": "?0",
  "sec-ch-ua-platform": '"Windows"',
  secure: "yes",
};

const axiosInstance = axios.create({
  headers: { ..._headers },
});

axiosInstance.interceptors.request.use(
  async (config) => {
    config.headers = {
      Accept: "application/json, text/plain, */*",
      Authorization: `Bearer ${_object.access_token || (await getToken())}`,
    };
    return config;
  },
  (error) => {
    Promise.reject(error);
  }
);

axiosInstance.interceptors.response.use(
  (response) => {
    return response;
  },
  async (error) => {
    let config = error.config;
    if (
      (error.response.status === 401 ||
        error.response.status === 403 ||
        error.response.status === 498 ||
        error.response.status === 500) &&
      !isRefreshing
    ) {
      isRefreshing = true;
      let new_token = "";
      try {
        new_token = await getToken();
      } catch {}
      isRefreshing = false;
      config.headers["Authorization"] =
        "Bearer " + (new_token || _object.access_token);
      return new Promise((resolve, reject) => {
        axios
          .request(config)
          .then((res) => {
            resolve(res);
          })
          .catch((err) => {
            reject(err);
          });
      });
    }
    return Promise.reject(error);
  }
);

const apiRequest = (path, config, method, payload) => {
  return axiosInstance.request({
    url: `https://${_host}/v1/${path}`,
    method: method,
    data: payload,
    ...config,
  });
};

const post = (path, config, payload) => {
  return axios.post(`https://${_host}/v1/${path}`, payload, {
    ...config,
  });
};

const checkToken = async () => {
  if (_object.access_token && Date.now() < _object.access_token_expires_in) {
    return _object.access_token;
  }
  return await getToken();
};

const onError = async (e) => {
  let regenToken = false;
  if (e && e.message && e.message.includes("code 401")) {
    await getToken();
    regenToken = true;
  }
  return regenToken;
};

const getToken = async () => {
  const obj = {
    grant_type: "password",
    username: "99228182",
    password: "VXVnaWkxMDMwKw==",
    channelId: "I",
    languageId: "003",
  };

  let res;
  try {
    res = await post(
      "cfrm/auth/token",
      {
        headers: {
          ..._headers,
        },
      },
      obj
    );
  } catch (error) {
    console.log(error);
  }
  if (
    res?.data &&
    (res.status === 200 || res.status === 201 || res.status === 202)
  ) {
    _object.access_token = res.data.access_token;
    _object.access_token_expires_in =
      getNow() + res.data.access_token_expires_in * 1000;
  }
  return _object.access_token;
};

const getTransactions = async () => {
  let today = moment();
  let yesterday = today.clone().subtract(1, "days");
  startDate = yesterday.format("YYYY-MM-DD");
  endDate = today.format("YYYY-MM-DD");

  let result = undefined;
  let res = undefined;

  let path = `omni/user/custom/operativeaccounts/${_account}/transactions?transactionValue=0&transactionDate={"lt":"${startDate}T00:00:00","gt":"${endDate}T00:00:00"}&amount={"lt":"0","gt":"0"}&amountType=04&transactionCategoryId=&transactionRemarks=&customerName=%20&transactionCurrency=MNT&branchCode=5692`;

  try {
    res = await apiRequest(path, {
      headers: {
        Authorization: "Bearer " + (await checkToken()),
      },
    });
  } catch (e) {
    if (await onError(e)) {
      res = await apiRequest(path, {
        headers: {
          Authorization: "Bearer " + (await checkToken()),
        },
      });
    }
  }

  if (
    res &&
    (res.status === 200 || res.status === 201 || res.status === 202) &&
    res?.data &&
    res.data instanceof Array
  ) {
    result = res.data;
  }
  return result;
};

app.listen(8000, async () => {
  let aa = await getTransactions();
  console.log("aa: ", aa);

  setInterval(async () => {
    aa = await getTransactions();
    console.log(aa);
  }, 120000);
});
